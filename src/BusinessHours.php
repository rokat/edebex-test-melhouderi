<?php
/**
 * BusinessHours
 * Class used to retrieve business requirements from a YAML configuration file
 * And uses the Holidays Object to check for holidays availability
 * And Computes the next Available Mail Sending Date
 */

namespace Melhouderi\Package;
use Symfony\Component\Yaml\Yaml;
use Carbon\Carbon;

class BusinessHours
{
    private $config;
    /** @var Holidays $holiday */
    private $holidays;
    private $businessConfig;

    private $weekdays = array(0=>"SUNDAY",1=>"MONDAY",2=>"TUESDAY",3=>"WEDNESDAY",4=>"THURSDAY",5=>"FRIDAY",6=>"SATURDAY");

    /**
     * BusinessHours constructor.
     * Construct the business hours contraints from a YAML
     * File to get the available work day and times
     * And if emails are allowed to be sent during that time
     * @param string $config
     *
     */
    public function __construct($config = 'config.yml')
    {
        $this->config = $config;
        $this->loadConfig();

    }

    /**
     * @param Holidays $holidays
     */
    public function setHolidays(Holidays $holidays){
        $this->holidays = $holidays;
    }

    /**
     * @return Holidays
     */
    public function getHolidays()
    {
        return $this->holidays;
    }

    /**
     * @param string $config
     */
    public function setConfig($config){
        $this->config = $config;
        $this->loadConfig();
    }

    /**
     * Function to re read the configuration file
     * Used to parse the YAML file
     */
    private function loadConfig(){
        $this->businessConfig = Yaml::parse(file_get_contents($this->config));
    }

    /**
     * @return mixed
     */
    public function getBusinessConfig()
    {
        return $this->businessConfig;
    }

    /**
     * @param string $date
     * @return string
     */
    public function mailTime($date){
        $dt = Carbon::parse($date);

        $bEmail = false;


        $this->computeSendDate($dt,$bEmail);

        if(!$bEmail){
            $dt = $this->availableEmailPeriod($dt);
        }
        return $dt->toDateTimeString();
    }

    /**
     * @param Carbon $date
     * @return string
     */
    private function getDayLabel(Carbon $date){
        $day = $date->dayOfWeek;
        //Convert Day (int) to Day String
        $dayLabel = $this->weekdays[$day];

        return $dayLabel;
    }

    /**
     * Retrive first available business day
     * Either the same day
     * Or Monday
     * @param Carbon $date
     * @return Carbon
     */
    private function nextBusinessDay(Carbon $date){
        $dayLabel = $this->getDayLabel($date);
        // By default we start on Monday
        // Or otherwise another day
        if(!isset($this->businessConfig['business-days'][$dayLabel])){
            $date->next(Carbon::MONDAY);
        }
        return $date;

    }


    /**
     * Function to compute the buisness the exact date & time to send the email,
     * and return whenever the period is allowed to send email or not
     * @param Carbon $date
     * @param bool $email
     * @return Carbon
     */
    private function computeSendDate(Carbon $date, bool &$email=false){

        // Retrieve the business times from configuration
        $bTime = $this->businessConfig['business-time'];

        // Convert Hours to minutes to ease computation and avoiding dealing with float
        $bMinutes = $bTime['hours'] * 60 + $bTime['minutes'];

        // Loop while there is remaining remaining minutes
        while($bMinutes>0){

            // We check for the first available buisness day
            $date = $this->nextBusinessDay($date);

            // Retrive the name of the DAY
            $dayLabel = $this->getDayLabel($date);

            $currentDay = $this->businessConfig['business-days'][$dayLabel];

            if(!$this->holidays->isHoliday($date)){
                foreach ($currentDay['periods'] as $period){
                    if($bMinutes>0){
                        // Hour is between a period
                        $start = $date->copy()->hour($period['start']['hour'])->minute($period['start']['minute']);
                        $end   = $date->copy()->hour($period['end']['hour'])->minute($period['end']['minute']);
                        if($date->between($start,$end)){
                            $bMinutes -= $end->diffInMinutes($date);
                            $date->hour = $end->hour;
                            $date->minute = $end->minute;
                            $email = $period['email'];
                        }
                        // Hour is before a period
                        elseif ($date->lt($start)){
                            $bMinutes -= $end->diffInMinutes($start);
                            $date->hour = $end->hour;
                            $date->minute = $end->minute;
                            $email = $period['email'];
                        }
                    }
                }
            }
            // We change the day to next day at 1AM (to avoid issues on holidays)
            if($bMinutes>0)
                $date->addDay()->hour(1)->minute(0);
        }
        // Add the remaining time to the Date (positive or negative)
        $date->addMinutes($bMinutes);
        return $date;
    }


    /**
     * Function to search the first allowed mail period to send email
     * regardless of remaining business hours,
     * as it should have  been computed
     * @param Carbon $date
     * @return Carbon
     */
    private function availableEmailPeriod(Carbon $date){
        $email = false;
        while(!$email) {
            // We check for the first available buisness day
            $date = $this->nextBusinessDay($date);

            // Retrive the name of the DAY
            $dayLabel = $this->getDayLabel($date);
            $currentDay = $this->businessConfig['business-days'][$dayLabel];

            if(!$this->holidays->isHoliday($date)){
                foreach ($currentDay['periods'] as $period){
                    // Hour is between period
                    $start = $date->copy()->hour($period['start']['hour'])->minute($period['start']['minute']);
                    $end   = $date->copy()->hour($period['end']['hour'])->minute($period['end']['minute']);
                    if(($date->lt($start) || ($date->between($start,$end))) && $period['email'])
                    {
                        $date->hour = $start->hour;
                        $date->minute = $start->minute;
                        return $date;

                    }
                }
            }
            $date->addDay()->hour(1)->minute(0);
        }
        return $date;
    }

    public function sendDate($date){
        $dt = Carbon::parse($date);
        return $dt;
    }




}