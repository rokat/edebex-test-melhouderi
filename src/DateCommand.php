<?php
/**
 * SYMFONY Command Used to Execute the application and testing
 * Without Requiring the need of an HTTP server
 */

namespace Melhouderi\Package;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class DateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('email:time')
            ->setDescription('Get the time to send the email')
            ->addArgument(
                'date',
                InputArgument::REQUIRED,
                'Date to send the email'
            )
            ->addArgument(
                'time',
                InputArgument::OPTIONAL,
                'Time to send the email'
            )
            ->addOption(
                'cal',
                null,
                InputOption::VALUE_OPTIONAL,
                'ICAL to be used for Holidays either file or url'
            )
            ->addOption(
                'config',
                null,
                InputOption::VALUE_OPTIONAL,
                'YAML Configuration file for business requirements'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $date = $input->getArgument('date');
        $time = $input->getArgument('time');
        $cal =$input->getOption('cal');
        $config = $input->getOption('config');
        
        if($time)
            $date = $date.' '.$time;
        
        if($cal)
            $bHolidays = new Holidays($cal);
        else $bHolidays = new Holidays();

        if($config)
            $bHours = new BusinessHours($config);
        else $bHours = new BusinessHours();

        $bHours->setHolidays($bHolidays);

        $output->writeln($bHours->mailTime($date));
    }
}