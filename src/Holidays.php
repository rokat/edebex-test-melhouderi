<?php
/**
 *
 * Holidays is a class used to retrive/parse ICAL events
 * Either by url or file path
 * Used to check if a day is a holiday
 */

namespace Melhouderi\Package;

use Carbon\Carbon;
use DateTime;
use om\IcalParser;
class Holidays
{
    private $icalSource;
    private $events;
    public function __construct($icalSource = "BEL.ics")
    {

        $this->icalSource = $icalSource;
        $parser = new IcalParser();
        $parser->parseFile($icalSource);
        $this->events = $parser->getEvents();
    }


    /**
     * Function used to check if the day is in the holidays
     * Holidays events are defined with a start and end date
     * We check if the date is between this interval
     * @param Carbon|DateTime $date
     * @return bool
     */

    public function isHoliday(Carbon $date){
        foreach ($this->events as $event){
            $start = Carbon::instance($event['DTSTART']);
            $end = Carbon::instance($event['DTEND']);
            if($date->between($start,$end)){
                return true;
            }
        }
        return false;
    }
    
    public function getEvents(){
        return $this->events;
    }
}