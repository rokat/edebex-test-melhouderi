# Edebex-Test-Melhouderi

Test for Edebex

Application Used to get the email sending time for a set of configuration

Installation
--

Clone this Repo

`git clone git@gitlab.com:rokat/edebex-test-melhouderi.git`

Then install composer dependencies with

`composer install`

Run
--

To execute the application

`php application email:time DATE TIME`

for example

`php application email:time 20-03-2016 9:00`

Inputs
--

Date can be entered in multiple variation either

`20-03-2016`

`2016/03/25`

`2016-03-25`

Time can be entered in other formats

`13:30`

`1pm`

`1:30pm`

It is possible to add other format of inputs with `Carbon::createFromFormat` according the documentation of [Carbon](http://carbon.nesbot.com/docs/)

Configuration
--

Configuration Files can be overridden

For Calendar holiday specify the option
`--cal="CALENDARURL or FILE"`

For Business hours configuration the option
`--config="FILE"`

Business Hours Configuration
--
It contains the configuration for business hours and business day and time required before sending email

The precision is in minutes

It's structure is as such
```YAML
# Second configuration file used for testing
# Possible Buisness Days to send email
 business-days:
# Buisness Day
  MONDAY :
  # Work Periods
    periods:
    # First Period
      1:
      # Start Period, End Period, Allowed to send email in this period
        start:
          hour: 9
          minute: 0
        end:
          hour: 12
          minute: 0
        email: false
    # Second Period
      2:
        start:
          hour: 13
          minute: 30
        end:
          hour: 17
          minute: 0
        email: true
#Buissness time required before sending email
 business-time:
  hours: 4
  minutes: 0
```
To add more days just add another day in the configuration as in [config.yml](config.yml)
Or change values according to the business requirements

Test
--
Tests have been included.
To run them use `phpunit`