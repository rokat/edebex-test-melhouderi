<?php
use Melhouderi\Package\BusinessHours;
use Melhouderi\Package\Holidays;

/**
 * Created by PhpStorm.
 * User: momo
 * Date: 20/03/16
 * Time: 22:36
 */
class BusinessHoursTest extends PHPUnit_Framework_TestCase
{


    public function testDefaultDate(){
        $bHolidays = new Holidays();
        $bHours = new BusinessHours();
        $bHours->setHolidays($bHolidays);
        $this->assertEquals('2016-03-14 14:30:00',$bHours->mailTime("2016/3/14 9:00"));
    }

    /**
     * Test if an email can be sent during a holiday (as it not possible)
     * The countdown must start at the next available date
     * In this example email is approved on "monday 28/3"
     * Email can only be sent the next day at 14:30  (9-12 + 13:30-14:30  = 4 Business Hours)
     */

    public function testDateHoliday(){
        $bHolidays = new Holidays();
        $bHours = new BusinessHours();
        $bHours->setHolidays($bHolidays);
        $this->assertEquals('2016-03-29 14:30:00',$bHours->mailTime("2016/3/28"));
    }

    /**
     * Test when an email is approved on friday at 9am
     * As no mail can be sent during 13:30 and 17:00
     * And monday morning emails cannot be sent during 9:00 - 12:00
     * It will be sent at 13:30
     */
    public function testFridayEmail(){
        $bHolidays = new Holidays();
        $bHours = new BusinessHours();
        $bHours->setHolidays($bHolidays);
        $this->assertEquals('2016-03-21 13:30:00',$bHours->mailTime("2016/3/18 9:00"));
    }


    /**
     * Test an approved email on friday at 9am
     * As by default no email can be sent during 13:30 and 17
     * The email should be sent on monday
     * But monday 28 is a Holiday so no mail can be sent
     * So email will be sent on tuesday
     */
    public function testFridayMondayHolidayEmail(){
        $bHolidays = new Holidays();
        $bHours = new BusinessHours();
        $bHours->setHolidays($bHolidays);
        $this->assertEquals('2016-03-29 09:00:00',$bHours->mailTime("2016/3/25 9:00"));
    }

    /**
     * Test another configuration file
     * where no mail can be sent on friday
     * Means approved mail on Tuesday afternoon will only be sent on Monday at 13:30
     */
    public function testConfigurationFile(){
        $bHolidays = new Holidays();
        $bHours = new BusinessHours('config-2.yml');
        $bHours->setHolidays($bHolidays);
        $this->assertEquals('2016-03-21 13:30:00',$bHours->mailTime("2016/3/17 13:00"));

    }
}