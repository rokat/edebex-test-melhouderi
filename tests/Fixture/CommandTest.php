<?php
use Melhouderi\Package\DateCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Testing Commands
 */
class CommandTest extends PHPUnit_Framework_TestCase
{


    public function testDateIsOutput()
    {
        $application = new Application();
        $application->add(new DateCommand());

        $command = $application->find('email:time');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
           'command'    => $command->getName(),
            'date'      => '22-2-2016',
            'time'      => '16:10'
        ));

        $this->assertRegExp('/2016-02-23 13:40:00/',$commandTester->getDisplay());
    }


    public function testOptions()
    {
        $application = new Application();
        $application->add(new DateCommand());

        $command = $application->find('email:time');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command'   => $command->getName(),
            'date'      => '22-2-2016',
            'time'      => '16:10',
            '--cal'     => 'BEL.ics',
            '--config'  => 'config.yml',
        ));

        $this->assertRegExp('/2016-02-23 13:40:00/',$commandTester->getDisplay());
    }

}