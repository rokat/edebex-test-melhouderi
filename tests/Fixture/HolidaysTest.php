<?php
use Melhouderi\Package\Holidays;

/**
 * Created by PhpStorm.
 * User: momo
 * Date: 20/03/16
 * Time: 22:42
 */
class HolidaysTest extends PHPUnit_Framework_TestCase
{


    /**
     * Test for holiday
     * Return True if that day is a holiday
     */
    public function testHolidaysDate(){
        $bHolidays = new Holidays();
        $this->assertTrue($bHolidays->isHoliday(\Carbon\Carbon::parse("2016-3-28")));
    }

    /**
     * Test Importation of External Calendar
     * For Example France Holiday Calendar
     * Check if 14/7/2016 is a holiday
     */
    public function testHolidaysFrance(){
        $bHolidays = new Holidays("http://www.kayaposoft.com/enrico/ics/v1.0?country=fra&fromDate=01-01-2016&toDate=31-12-2016&en=1");
        $this->assertTrue($bHolidays->isHoliday(\Carbon\Carbon::parse("2016-7-14")));

    }
}